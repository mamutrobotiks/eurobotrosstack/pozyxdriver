from setuptools import find_packages, setup

package_name = 'pozyx_driver'

setup(
    name=package_name,
    version='0.1.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Guelakais',
    maintainer_email='koroyeldiores@gmail.com',
    description='TODO: Package description',
    license='Apache2',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'pozyxNode = pozyx_driver.pozyxNode:main'
        ],
    },
)
