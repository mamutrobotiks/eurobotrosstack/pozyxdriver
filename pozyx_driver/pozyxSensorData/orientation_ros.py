from time import time
from sensor_msgs.msg import Imu
from pypozyx.definitions.bitmasks import POZYX_INT_MASK_IMU
from pypozyx import SensorData, SingleRegister, POZYX_SUCCESS

class Orientation3D:
    def __init__(self, pozyx, node, remote_id=None):
        self.pozyx = pozyx
        self.remote_id = remote_id
        self.node = node
        self.current_time = time()
        self.imu_publisher = self.node.create_publisher(Imu, '/Pozyx_imu', 10)

    def loop(self):
        sensor_data = SensorData()
        calibration_status = SingleRegister()
        if self.remote_id is not None or self.pozyx.checkForFlag(POZYX_INT_MASK_IMU, 0.01) == POZYX_SUCCESS:
            status = self.pozyx.getAllSensorData(sensor_data, self.remote_id)
            status &= self.pozyx.getCalibrationStatus(calibration_status, self.remote_id)
            if status == POZYX_SUCCESS:
                self.publishSensorData(sensor_data, calibration_status)

    def publishSensorData(self, sensor_data, calibration_status):
        imu_msg = Imu()
        imu_msg.header.stamp = self.node.get_clock().now().to_msg()
        imu_msg.orientation.x = sensor_data.quaternion.w
        imu_msg.orientation.y = sensor_data.quaternion.x
        imu_msg.orientation.z = sensor_data.quaternion.y
        imu_msg.orientation.w = sensor_data.quaternion.z
        imu_msg.angular_velocity.x = sensor_data.angular_vel.x
        imu_msg.angular_velocity.y = sensor_data.angular_vel.y
        imu_msg.angular_velocity.z = sensor_data.angular_vel.z
        imu_msg.linear_acceleration.x = sensor_data.acceleration.x
        imu_msg.linear_acceleration.y = sensor_data.acceleration.y
        imu_msg.linear_acceleration.z = sensor_data.acceleration.z
        self.imu_publisher.publish(imu_msg)