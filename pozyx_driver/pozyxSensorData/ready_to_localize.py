#!/usr/bin/env python3
from geometry_msgs.msg import Point
from time import sleep
from pypozyx import (POZYX_POS_ALG_UWB_ONLY, POZYX_3D, Coordinates, POZYX_SUCCESS, PozyxConstants,
                     SingleRegister, 
                     DeviceList, PozyxRegisters)

class RobotPosition:
    def __init__(self, pozyx, node, anchors, algorithm=POZYX_POS_ALG_UWB_ONLY, dimension=POZYX_3D, height=1000, remote_id=None):
        self.pozyx = pozyx
        self.node = node
        self.anchors = anchors
        self.algorithm = algorithm
        self.dimension = dimension
        self.height = height
        self.remote_id = remote_id
        self.anchorPublisher=self.node.create_publisher(Point,"robot_position",10)

    def setup(self):
        self.setAnchorsManual()
        for anchor in self.anchors:
            self.node.get_logger().info(f"ANCHOR,{'0x%0.4x'%anchor.network_id},{anchor.pos}")

    def loop(self):
        position = Coordinates()
        if not(self.pozyx.doPositioning(position, self.dimension, self.height, self.algorithm, remote_id=self.remote_id) == POZYX_SUCCESS):
            self.printPublishErrorCode("positioning")
        else:
            self.printPublishPosition(position)

    def printPublishPosition(self, position):
        network_id = self.remote_id
        if network_id is None:
            network_id = 0
        if self.node is not None:
            anchor_msg = Point()
            anchor_msg.x = position.x
            anchor_msg.y = position.y
            anchor_msg.z = position.z
            self.anchorPublisher.publish(anchor_msg)

    def printPublishErrorCode(self, operation):
        error_code = SingleRegister()
        network_id = self.remote_id
        status = POZYX_SUCCESS
        if network_id is None:
            self.pozyx.getErrorCode(error_code)
        else:
            status = self.pozyx.getErrorCode(error_code, self.remote_id)
        error_msg = self.pozyx.getErrorMessage(error_code)
        if network_id is None or status == POZYX_SUCCESS:
            self.node.get_logger().info(f"{'LOCAL ' if network_id is None else ''}ERROR {operation}" +
                  (f" on ID {'0x%0.4x' % network_id}" if network_id is not None else "") +
                  f", {error_msg}")
        else:
            self.node.get_logger().info(f"ERROR {operation}, couldn't retrieve remote error code, LOCAL ERROR {error_msg}")
            network_id = 0
            error_code[0] = -1
        self.node.get_logger().info(f"Error,{operation}{network_id} {error_code[0]}"if self.node is not None else '')

    def setAnchorsManual(self, save_to_flash=False):
        status = self.pozyx.clearDevices(remote_id=self.remote_id)
        status &= all(self.pozyx.addDevice(anchor, remote_id=self.remote_id) for anchor in self.anchors)
        if len(self.anchors) > 4:
            status &= self.pozyx.setSelectionOfAnchors(PozyxConstants.ANCHOR_SELECT_AUTO, len(self.anchors),
                                                       remote_id=self.remote_id)
        if save_to_flash:
            self.pozyx.saveAnchorIds(remote_id=self.remote_id)
            self.pozyx.saveRegisters([PozyxRegisters.POSITIONING_NUMBER_OF_ANCHORS], remote_id=self.remote_id)
        return status
