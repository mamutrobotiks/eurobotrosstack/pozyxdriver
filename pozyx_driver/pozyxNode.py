import rclpy
from pozyx_driver.pozyxSensorData.orientation_ros import Orientation3D
from pozyx_driver.pozyxSensorData.ready_to_localize import RobotPosition
from pypozyx import (
    Coordinates,DeviceCoordinates,get_first_pozyx_serial_port,PozyxConstants,
    PozyxSerial)
from pypozyx.tools.version_check import perform_latest_version_check

class EnvironmentConstants:
    remoteId=0x764e
    height=1000
    anchors=[
        DeviceCoordinates(0x765c, 1, Coordinates(129, -88, 660)),
        DeviceCoordinates(0x763b, 1, Coordinates(1535, 2113, 660)),
        DeviceCoordinates(0x760a, 1, Coordinates(2965, 834, 1040)),
        DeviceCoordinates(0xA004, 1, Coordinates(2968, -54, 660))
    ]
    algorithm=PozyxConstants.POSITIONING_ALGORITHM_UWB_ONLY
    dimension=PozyxConstants.DIMENSION_3D



class PozyxPublisher:
    def __init__(self,node,remote_id,height,algorithm,pozyxPort,dimension,anchors):
        if pozyxPort is None:
            node.get_logger().info("No Pozyx connected. Check your USB cable or your driver!")
            quit()
        self.node=node
        self.remote_id=None
        self.height=height
        self.algorithm=algorithm
        self.pozyx=PozyxSerial(pozyxPort)
        self.dimension=dimension
        self.anchors=anchors

    def setup(self):
        self.imu_publisher = Orientation3D(
            self.pozyx, self.node, self.remote_id
            )
        self.position_publisher=RobotPosition(
            self.pozyx, self.node,self.anchors,self.algorithm,self.dimension,
            self.height,self.remote_id
        )
    def publish(self):
        self.imu_publisher.loop()
        self.position_publisher.setup()
        self.position_publisher.loop()


def main(args=None):
    perform_latest_version_check()
    rclpy.init(args=args)
    minimal_publisher = PozyxPublisher(
        rclpy.create_node('PozyxPublisher'),
        EnvironmentConstants.remoteId,
        EnvironmentConstants.height,
        EnvironmentConstants.algorithm,
        get_first_pozyx_serial_port(),
        EnvironmentConstants.dimension,
        EnvironmentConstants.anchors
        )
    minimal_publisher.setup()
    while rclpy.ok():
        minimal_publisher.publish()
    minimal_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()